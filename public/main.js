const markdownRoot = document.getElementById("markdown-root");
let md = window.markdownit();

fetch("content.md", { mode: "no-cors" })
  .then((response) => response.text())
  .then((data) => {
    let renderedMarkdown = md.render(data);
    markdownRoot.innerHTML = renderedMarkdown;
  })
  .catch((error) => console.error(error));
