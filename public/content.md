## Example content in the Markdown file

This is some example content of the markdown file :)

[The Repository on Gitlab](https://gitlab.com/Anne-Victoria/floss-simulation-website)

A grey tabby cat that is lying down looking right at you:


![A grey tabby cat that is lying down looking right at you](images/cat.jpg)


Credit: The photo is by Inge Wallumrød on [Unsplash](https://www.pexels.com/photo/silver-tabby-cat-lying-on-brown-wooden-surface-126407/)
